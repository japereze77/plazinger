// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD_OwJa-6rWHWt2o-3fFxQbz-IN6SkZfU4",
    authDomain: "platzinger-fa9af.firebaseapp.com",
    databaseURL: "https://platzinger-fa9af.firebaseio.com",
    projectId: "platzinger-fa9af",
    storageBucket: "platzinger-fa9af.appspot.com",
    messagingSenderId: "1017060913821",
    appId: "1:1017060913821:web:2731be1c0c592826cc5a74",
    measurementId: "G-7N20F1VDJC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

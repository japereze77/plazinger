import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
operation :string = 'login';
email:string = null
password:string = null
nick:string = null

  constructor(private authenticationService: AuthenticationService,private userService:UserService, private router:Router) { }

  ngOnInit() {
  }

  login(){
    this.authenticationService.loginWithEmail(this.email, this.password ).then(
      (data) => {  
        //Fin del registroy logeado
          alert('Logeado correctamente');
          console.log(data);
         this.router.navigate(['/home'])
      }).catch(
        (error) => {
          alert('Ocurrio un error en el logeo');
          console.log(error);
      });
  }

  register(){
    this.authenticationService.registerWithEmail(this.email, this.password ).then(
        (data) => {
         //se implementan lo campos 
          const user = {
            uid: data.user.uid,
            email: this.email,
            nick: this.nick
        };
        //Se genera el usuario 
        this.userService.createUser(user).then((dataUser)  =>{
          alert('registrado correctamente');
          console.log(dataUser) 
          this.email =''
          this.password=''
          this.nick=''
        }).catch(
          (error) => {
            alert('Ocurrio un error en el registro');
            console.log(error);
        });
        //Fin del registro corecto
          
      }).catch(
        (error) => {
          alert('Ocurrio un error en el registro : ' + error);
          console.log(error);
      });
  }

}

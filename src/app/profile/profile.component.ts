import { Component, OnInit } from '@angular/core';
import { user } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
user:user;
imageChangedEvent: any = '';
croppedImage: any = '';
picture: any;

    

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  
  imageCropped(event: ImageCroppedEvent) {

    this.croppedImage = event.base64;
  }
  
  imageLoaded() {
    // show cropper
  }
  
  cropperReady() {
    // cropper ready
  }
  
  loadImageFailed() {
    // show message
  }


  constructor(private userService:UserService, private authenticationService:AuthenticationService,private fbstorage:AngularFireStorage) {
    this.authenticationService.getStatus().subscribe((status)=>{
      this.userService.getUserById(status.uid).valueChanges().subscribe((data:user)=>{
        this.user = data
        console.log(this.user)
      },
      (error)=>{
        console.log(error)
      })
    },
    (error)=>{
      console.log(error)
    });
   }

  ngOnInit() {
  }

  saveSettings(){
    if(this.croppedImage){ //imegen recortada
      const currentPictureId= Date.now()//nombre unico
      const pictures = this.fbstorage.ref('pictures/'+currentPictureId+'.jpg').putString(this.croppedImage,'data_url'); //subio la imagen
      pictures.then((result)=>{
        this.picture = this.fbstorage.ref('pictures/'+currentPictureId+'.jpg').getDownloadURL();//url hacia el archivo binario
        this.picture.subscribe((p)=>{
          this.userService.setAvatar(p, this.user.uid).then(()=>{ //ser recibe la url y va y busca la imagen 
            alert('Avatar subido correctamente')
          }).catch((error)=>{
            alert('Hubo un error al subir la imagen');
            console.log(error)
          });
        })
      }).catch((error)=>{
        alert(error);
      });
    }else{
      this.userService.editUser(this.user).then(()=>{
        alert('Cambios Guardados')
      }).catch((error)=>{
        alert(error);
      });
    }
    }

   
}

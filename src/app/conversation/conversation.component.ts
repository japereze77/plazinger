import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { user } from '../interfaces/user';
import {AngularFireDatabase} from '@angular/fire/database';
import { ConversationService } from '../services/conversation.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {
  friendId: any;
  friend:user;
  price: number = 78.555564564;
  today: any = Date.now();
  user:user;

  constructor(private activatedRoute:ActivatedRoute, private userService:UserService, 
    private anfdta:AngularFireDatabase, private conversationService:ConversationService, private authenticationService: AuthenticationService) {
    

      this.friendId = this.activatedRoute.snapshot.params['uid']
  
      this.userService.getUserById(this.friendId).valueChanges().subscribe(
        (data: user) =>{
          this.friend = data;
        },(error) =>{
          console.log(error)
        });
        // se obtiene el uid de la sesión
      this.authenticationService.getStatus().subscribe((sesion) => {
        /*this.userService.getUserById(sesion.uid).valueChanges( (user: User) => {
          this.user = user;
        })*/
      })
   }

  ngOnInit() {
  }

  setMessage(){
    const message = {
      uid:this.user.uid,
      timestamp:Date.now()
    }

    this.conversationService.createConversation(message);
  }

}

import { Component, OnInit } from '@angular/core';
import{ user } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  friends: user[];
  query:string = '';

  constructor(private userService:UserService, private authenticationService: AuthenticationService,private router:Router ) {
    console.log('Ok')
    this.userService.getUsers().valueChanges().subscribe(
      (data: user[]) =>{
        this.friends = data;
      },(error) =>{
        console.log(error)
      });
    
   }

  ngOnInit() {
 
  }

  logout(){
  /*this.authenticationService.logout().then( () => {
      alert('Sesión Cerrada');
      this.router.navigate(['/login'])
    }).catch((error)=>{
      alert("Error al cerrar sesión");
      console.log(error)
    });*/


    this.authenticationService.logout();
    alert('Sesión cerrada correctamente');
    this.router.navigate(['/login'])
  }

}

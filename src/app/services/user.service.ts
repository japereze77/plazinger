import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private anfdta:AngularFireDatabase) {}

  getUsers(){
    return this.anfdta.list('/users');
  }

  getUserById(uid){
    return this.anfdta.object('/users/'+uid);
  }

  createUser(user){
    return this.anfdta.object('/users/'+user.uid).set(user);
  }

  editUser(user){
    return this.anfdta.object('/users/'+user.uid).set(user);
  }
   
  setAvatar(avatar,uid){
   return this.anfdta.object('/users/'+uid+'/avatar').set(avatar)
  }
}

import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { timestamp } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  constructor(private angurlarFDB: AngularFireDatabase) { }

  createConversation(conversation){
    return this.angurlarFDB.object('conversations/'+ conversation.uid + '/' + conversation.timestamp).set(conversation);
  }

  getConversation(uid){
    return  this.angurlarFDB.list('conversations/'+uid);
  }

}

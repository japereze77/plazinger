import { Injectable } from '@angular/core';
import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-root',
  template: `
    <div *ngIf="afAuth.user | async as user; else showLogin">
      <h1>Hello {{ user.displayName }}!</h1>
      <button (click)="logout()">Logout</button>
    </div>
    <ng-template #showLogin>
      <p>Please login.</p>
      <button (click)="login()">Login with Google</button>
    </ng-template>
  `,
})

export class AuthenticationService {

  constructor(private afAuth: AngularFireAuth) {
  }

loginWithEmail(email:string, password:string){
  return this.afAuth.auth.signInWithEmailAndPassword(email,password);
}

registerWithEmail(email:string, password:string){
  return this.afAuth.auth.createUserWithEmailAndPassword(email,password);
}

getStatus(){
  return  this.afAuth.authState;
}

  login() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }
  logout() {
    this.afAuth.auth.signOut();
  }
}
